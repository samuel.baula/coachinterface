import requests
import json

import time
from time import sleep
import re
import sys
import os

from google.cloud import texttospeech
import wave
from pygame import mixer
import subprocess
import threading

os.environ[
   "GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/nestore/credentials.json"

# Audio recording parameters
RATE = 16000
CHUNK = 1024
# CHUNK = int(RATE / 10*3 )  # 100ms
RECORD_SECONDS = 5
CHANNELS = 2

address = "http://192.168.43.167:9000/"
lang_id = "en-GB"

x = requests.get(address+"getPattern")
res = x.text
jres = json.loads(res)

test = "Hello, this is a test sentence to ajust the sound and the image"

#  synthesis_input = texttospeech.types.SynthesisInput(text=apiresp)
voice = texttospeech.types.VoiceSelectionParams(
	language_code=lang_id,
	ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)
# print(rt.text)
audio_config_1 = texttospeech.types.AudioConfig(
	audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16, sample_rate_hertz=99000)


def thread_function(name):
	if(jres['sound'] != "None"):
		print(jres['sound'])
		test = jres['sound']
		client_1 = texttospeech.TextToSpeechClient()
		synthesis_input = texttospeech.types.SynthesisInput(text=test)

		response_2 = client_1.synthesize_speech(synthesis_input, voice, audio_config_1)

		wav = wave.open('/home/pi/coachinterface/recording.wav', 'w')
								
		wav.setnchannels(CHANNELS)
		wav.setsampwidth(2)
		wav.setframerate(44100)

		wav.writeframes(response_2.audio_content)

		print('Audio content written to file "recording.wav"')

		wav.close()

		os.system("aplay -D hw:2,1 /home/pi/coachinterface/recording.wav")
		#sub = subprocess.Popen([ "aplay -D hw:2,1 /home/pi/coachinterface/recording.wav" ], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT )
		#sub.communicate()

x = threading.Thread(target=thread_function, args=(1,))
x.start()

with open("/home/pi/coachinterface/test_pattern.py", 'w') as f:
	f.write(jres["code"])

from test_pattern import execute_pattern

length = len(jres["images"])
times = jres["times"]
time = 0
for i in range(0, length):
	time += times[i]

print(time)
oneLoopTime = time

nbIter = 2
while time < 10:
	nbIter += 1
	time += oneLoopTime

for i in range(0, nbIter):
	execute_pattern()
	print(i)
	sleep(0.1)


os.system("python /home/pi/coachinterface/reset.py")