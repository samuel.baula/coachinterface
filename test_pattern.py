from matrix_lite import led
import time
from time import sleep
#init
everloop = ['black'] * led.length
everloop[0] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[1] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[2] = {'r':231, 'g':204, 'b':1, 'w':0 }
everloop[3] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[4] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[5] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[6] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[7] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[8] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[9] = {'r':231, 'g':204, 'b':1, 'w':0 }
everloop[10] = {'r':231, 'g':204, 'b':1, 'w':0 }
everloop[11] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[12] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[13] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[14] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[15] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[16] = {'r':0, 'g':0, 'b':0, 'w':0 }
everloop[17] = {'r':231, 'g':204, 'b':1, 'w':0 }
led.set(everloop)
i = 0
def execute_pattern():
	global i
	if i == 0 :
		everloop[0] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[1] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[2] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[3] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[4] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[5] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[6] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[7] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[8] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[9] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[10] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[11] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[12] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[13] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[14] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[15] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[16] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[17] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		i += 1
		sleep(0.8)
	if i == 1 :
		everloop[0] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[1] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[2] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[3] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[4] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[5] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[6] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[7] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[8] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[9] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[10] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[11] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[12] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[13] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[14] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[15] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[16] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[17] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		i += 1
		sleep(0.8)
	if i == 2 :
		everloop[0] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[1] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[2] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[3] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[4] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[5] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[6] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[7] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[8] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[9] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[10] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[11] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[12] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[13] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[14] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[15] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[16] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[17] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		i += 1
		sleep(0.8)
	if i == 3 :
		everloop[0] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[1] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[2] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[3] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[4] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[5] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[6] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[7] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[8] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[9] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[10] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[11] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[12] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[13] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[14] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[15] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		everloop[16] = {'r':231, 'g':204, 'b':1, 'w':0 }
		led.set(everloop)
		everloop[17] = {'r':0, 'g':0, 'b':0, 'w':0 }
		led.set(everloop)
		i += 1
		sleep(0.8)
	if i == 4 :
		i = 0
