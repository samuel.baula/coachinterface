
from __future__ import division
import requests

import re
import sys
import json
import os

from everloop2 import pixels

import time

from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import pyaudio
from six.moves import queue

import checkTangData
from checkTangData import userprofile
from notification import saveNotification
#import random
#random.seed( 30 )
from google.cloud import texttospeech
import wave
import signal
def signal_handler():
    raise IOError("kill")
signal.signal(signal.SIGINT, signal_handler)

import board
import digitalio
import busio
import adafruit_lis3dh
from somefunctions import fixstring, remove_nestore
i2c = busio.I2C(board.SCL, board.SDA)
int1 = digitalio.DigitalInOut(board.D6)  # Set this to the correct pin for the interrupt!
lis3dh = adafruit_lis3dh.LIS3DH_I2C(i2c, int1=int1)

# Audio recording parameters
RATE = 16000
CHUNK = 1024
# CHUNK = int(RATE / 10*3 )  # 100ms
RECORD_SECONDS = 5
CHANNELS = 2

os.environ[
   "GOOGLE_APPLICATION_CREDENTIALS"] = "/home/pi/nestore/credentials.json"
   
client = speech.SpeechClient()
client_1 = texttospeech.TextToSpeechClient()
class Error(Exception):
    pass


class accelerometergoesoff(Error):
    pass


class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""

    def __init__(self, rate, chunk):
        self._rate = rate
        self._chunk = chunk

        # Create a thread-safe buffer of audio data

        self._buff = queue.Queue()
        self.closed = True
        self.time= 15
        self.timefinished = 0
        self.stop = 0
        self.testPattern = 0
        self.acc= 0
        self.error= False
        self.exception= " "
        self.recordingtime=2
        self.googletime=userprofile.testgoogletime()
        

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            # The API currently only supports 1-channel (mono) audio
            # https://goo.gl/z757pE
            channels=1, rate=self._rate,
            input=True, frames_per_buffer=self._chunk,
            # Run the audio stream asynchronously to fill the buffer object.
            # This is necessary so that the input device's buffer doesn't
            # overflow while the calling thread makes network requests, etc.
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""

        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            print(self.time)
            #self.accelerometer()
            self.time= self.time-1
            time.sleep(1)
            if self.time < RECORD_SECONDS:
                self.time= 15

                self.timefinished = 1

                break
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)

                    if chunk is None:
                        return
                    data.append(chunk)

                except queue.Empty:
                    break

            yield b''.join(data)


       
    def configuration(self):
        
        


        self.lang_id,self.usertoken, self.stop = userprofile.testuserfile()
#        print("toekn"+ self.usertoken)        
        self.URL="https://api.nestore-coach.eu/prod/hesso/coach/chatbot/tangible/message?lang="+self.lang_id 


        self.config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=RATE,
            language_code=self.lang_id,
            enable_word_time_offsets=True,
            speech_contexts=[speech.types.SpeechContext(
                phrases=['nutritional', 'yes', 'no', 'physical', 'feelings', 'emotions', 'home', 'nestore'],
            )],

        )
        self.streaming_config = types.StreamingRecognitionConfig(
            config=self.config,
            interim_results=True,
            single_utterance=True)
 

        #  synthesis_input = texttospeech.types.SynthesisInput(text=apiresp)
        self.voice = texttospeech.types.VoiceSelectionParams(
            language_code=self.lang_id,
            ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE)
        # print(rt.text)
        self.audio_config_1 = texttospeech.types.AudioConfig(
            audio_encoding=texttospeech.enums.AudioEncoding.LINEAR16, sample_rate_hertz=99000)








    def listen(self):




               
                pixels.listen_nestore()
                isresponse = False
                with MicrophoneStream(RATE, CHUNK) as stream:
                   

                        audio_generator = stream.generator()
                        print("say something")



 #                       time.sleep(0.1)
           
                        try:
                                requests_1 = (types.StreamingRecognizeRequest(audio_content=content)
                                              for content in audio_generator)


                               
                                responses = client.streaming_recognize(self.streaming_config, requests_1)
                               

                                for response in responses:

                                    for result in response.results:
                                        # print("question"+result.alternatives[0].transcript)

                                        if (result.is_final == True):
                                            stream.__exit__(None, None, None)
                                            question = result.alternatives[0].transcript
                                            isresponse = True
                                            print("question" + question)
                                            question= remove_nestore(question)
                                            return question

                                if (stream.timefinished == 1 and not isresponse):
                                    self.stop = 1
                                    self.time= 30
                                    self.error= True
                                    self.exception="noinput"
                                    pass
                        except Exception as e:
                            print(e)
                            print("error in listen")
                            
                            self.stop=1
                            #pixels.off_nestore()
                            self.error= True
                            self.exception="general"
                            pass
                            
                           # question="google api tech problems"
                            #question=" goodbye"
                            #return question


    def think(self, question):

                  if not self.error:
             #               time.sleep(0.5)             
                            pixels.think_nestore()
                            #print("question " + "here")
                            headers = {"Authorization": "bearer" + str(self.usertoken), "Content-Type":"application/json; charset=utf-8"}


                            #Add trigger for executing emotion interface script
                            print("test question :")
                            print(question)
                            if question == "emotional interface":
                                pixels.clear()
                                print("test trigger")
                                os.system("python /home/pi/coachinterface/getPattern.py")
                                print("stop")
                                self.testPattern = 1
                            else:
                        
                          
                                try:
                        
                                        req = requests.post(self.URL,
                                                            json={"text": question}, headers=headers)
                                                            
                                                        

                                        j = json.loads(req.text)
                        
                                        apiresp = j["text"]
                                        
                                        answer, text2=fixstring(apiresp)
                                        
                                        if (text2 and text2!='0'):
                                            print("text is very big 94449039839832894389893893892389")
                                        
                                            self.recordingtime=1
                                        else: 
                                            self.recordingtime=2
                                    
                                        if 'discussionEnd' in j:
                                            if j["discussionEnd"] == 1:
                                                print("stop")
                                                self.stop = 1
                                    
                                        
                                    
                                        synthesis_input = texttospeech.types.SynthesisInput(text=answer)

                                        response_2 = client_1.synthesize_speech(synthesis_input, self.voice, self.audio_config_1)

                                        wav = wave.open('/home/pi/nestore/recording.wav', 'w')
                                
                                        wav.setnchannels(CHANNELS)
                                        wav.setsampwidth(2)
                                        wav.setframerate(44100)
                            
                                        wav.writeframes(response_2.audio_content)
                                        
                                        print('Audio content written to file "recording.wav"')
                                        
                                        if(self.recordingtime==1):
                                            print ("heheheheeheheheh")
                                            self.bigtext(text2)
                                        wav.close()
                        


                                except requests.exceptions.ConnectionError:
                                    self.error=True
                                    self.exception="con"
                                    print("int connection gone")   #error2
                                    pass
                                except requests.exceptions.InvalidHeader as t:
                                    print(t)
                                    self.error= True
                                    self.exception="token"
                                    print("tokenerror")
                                    pass
                                except json.decoder.JSONDecodeError as d:
                                    print(d)
                                    self.error=True
                                    self.exception="token"
                                    print("token wrong")   #error2
                                    pass
                                except requests.exceptions.RequestException as t:
                                    self.error=True
                                    self.exception="general"  #error3
                                    pass

                                except Exception as e:
                                    print("Error in think")
                                    print("general exception"+ str(e))         #error4
                                    self.error=True
                                    self.exception="general"

                                    pass
 
                            #pixels.off_nestore() ###play sound that there is no internet 
                      
    def bigtext(self, text2):
                synthesis_input = texttospeech.types.SynthesisInput(text=text2)

                response_2 = client_1.synthesize_speech(synthesis_input, self.voice, self.audio_config_1)

                wav = wave.open('/home/pi/nestore/recording2.wav', 'w')
                               
                wav.setnchannels(CHANNELS)
                wav.setsampwidth(2)
                wav.setframerate(44100)
                           
                wav.writeframes(response_2.audio_content)
                                    
                print('Audio content written to file "recording2222222222222222222222222.wav"')
                                    
                      
                      
    def speak(self): 
                  print("test1")
                  if self.testPattern :
                            print("test2")
                            self.stop=1
                            self.testPattern=0
                            return
                  pixels.speak_nestore()           
                  self.accelerometer()
          #        pixels.off_nestore()
              #    pixels.clear()
                  if not self.acc:
                            print("i am at speak session stop is 0")
                            print(self.stop)
                  #          pixels.speak_nestore()
                            
                            if(not self.error):
                                 os.system("aplay -D hw:2,1 /home/pi/nestore/recording.wav")
                                 if (self.recordingtime==1):
                                     os.system("aplay -D hw:2,1 /home/pi/nestore/recording2.wav")
                                     
                            else:
                                self.error=False
                                self.stop=1
                                os.system("aplay -D hw:2,1 /home/pi/nestore/voices/error-"+self.exception+"-"+self.lang_id+".wav")
                            #pixels.off_nestore()
                  else:
               #              pixels.forceclean()
                             print("i am at speak session and acc is saying sleep stop 1")
                             #print(self.stop)
                             #self.stop=1
                             #pixels.offing()
                             
                                

    def conversation(self):
         self.stop = 0
         self.acc=0
         
 
         #self.configuration()
       
         print("notification", self.getnotification())
         self.notificationConversation(self.getnotification())
         
         print("if it is 1 then stop ----", self.stop)
         
         
         while not self.stop and not self.acc:
            #with MicrophoneStream(RATE, CHUNK) as conv:

                question=self.listen()
                self.think(question)
                self.speak()
                self.accelerometer()

         pixels.off_nestore()

    def notificationConversation(self, Isnotification):
        if(Isnotification==1):
            print("i am here")
            saveNotification("emotion", 0) 
            question= "end_of_day"
            self.think(question)
            self.speak()
            
    def getnotification(self):
       if(os.path.exists('/home/pi/nestore/notification.json')):
         with open('/home/pi/nestore/notification.json') as jsonfile:
             data= json.load(jsonfile)
             Isnotification=data['emotion']
             return Isnotification
       else:
         print("from nestore_mic no notification file")
         return 0


    def resetconversation(self):
        headers = {"Authorization": self.usertoken}
                          
        try:
                    
                req = requests.post(self.URL,
                json={"text": "sleepstate"}, headers=headers)
                print("convo resetted")
        except: 
                print("exception on reset convo")
                pass


    def accelerometer(self):
        
          if(lis3dh.acceleration[2]>8 and lis3dh.acceleration[2]<11): 
              time.sleep(0.5)
              self.acc=1
              #pixels.off_nestore()
              print("i am in accelerometer function...jai donne stop 1")
          else:
              self.acc=0
        
nestoreconv = MicrophoneStream(RATE, CHUNK)
#nestoreconv.configuration()

#if __name__ == '__main__':

#    nestoreconv.conversation()
#

